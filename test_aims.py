import aims
from nose.tools import assert_almost_equal

def test_positives():
    lst =[2,2]
    ab = aims.std(lst)
    exp = 0
    assert_almost_equal(ab, exp)


def test_negatives():
    lst =[-3,-5]
    ab = aims.std(lst)
    exp = 1.0
    assert_almost_equal(ab, exp)


def test_float():
    lst =[0.3,0.5]
    ab = aims.std(lst)
    exp = 0.1
    assert_almost_equal(ab, exp)

def test_negative_positive():
    lst =[-1,3]
    ab = aims.std(lst)
    exp = 2
    assert_almost_equal(ab, exp)
